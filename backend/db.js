const mongoose = require("mongoose");

mongoose.connect("mongodb+srv://ali_ariz:YKB7C6J80lA1V2WS@cluster0.7lmvm.mongodb.net/BlockChain?retryWrites=true&w=majority", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

const paymentSchema = new mongoose.Schema({
  id: String,
  itemId: String,
  paid: Boolean,
},{
  timestamps: true
});

const Payment = mongoose.model("Payment", paymentSchema);

module.exports = {
  Payment,
};
