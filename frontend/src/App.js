import React, { useState, useEffect } from "react";
import axios from "axios";
import { ethers, Contract } from "ethers";
import PaymentProcessor from "./contracts/PaymentProcessor.json";
import Dai from "./contracts/Dai.json";

const API_URL = "http://localhost:4000";

const ITEMS = [
  {
    id: 1,
    price: ethers.utils.parseEther("100"),
  },
  {
    id: 2,
    price: ethers.utils.parseEther("200"),
  },
];

function App() {
  const [pp, setPP] = useState(undefined);
  const [dd, setDD] = useState(undefined);
  const [stopper, setStopper] = useState(false);

  useEffect(() => {
    window.addEventListener("load", async () => {
      if (window.ethereum) {
        await window.ethereum.enable();
        const provider = new ethers.providers.Web3Provider(window.ethereum);
        const signer = provider.getSigner();

        let networkId = window.ethereum.networkVersion;
        const paymentProcessor = new Contract(
          PaymentProcessor.networks[networkId].address,
          PaymentProcessor.abi,
          signer
        );

        const dai = new Contract(
          Dai.networks[networkId].address,
          Dai.abi,
          signer
        );

        setPP(paymentProcessor);
        setDD(dai);
      } else {
        setStopper(!stopper);
      }
    });
  }, [stopper]);

  const buy = async (item) => {
    if (!pp) {
      alert("Payment Processor is undefined");
      return;
    }

    if (!dd) {
      alert("Dai is undefined");
      return;
    }
    const response1 = await axios.get(API_URL + "/api/getPaymentId/" + item.id);
    console.log(response1);
    const tx1 = await dd.approve(pp.address, item.price);
    await tx1.wait();

    const tx2 = await pp.pay(item.price, response1.data.paymentId);
    await tx2.wait();

    await new Promise((resolve) => setTimeout(resolve, 5000));
    const response2 = await axios.get(
      API_URL + "/api/getItemUrl/" + response1.data.paymentId
    );
    console.log(response2);
  };

  if (typeof window.ethereum === "undefined") {
    return (
      <div className="container">
        <div className="col-sm-12">
          <h1>Blockchain App</h1>
          <p>You need to install the latest version of Metamask</p>
        </div>
      </div>
    );
  }
  return (
    <div className="container">
      <div className="col-sm-12">
        <h1>Blockchain App</h1>
        <ul className="list-group">
          <li className="list-group-item">
            Buy Item1 - <span className="font-weight-bold">100 DAI</span>
            <button
              style={{ marginLeft: "10%" }}
              type="button"
              className="btn btn-primary pull-right"
              onClick={() => buy(ITEMS[0])}
            >
              Buy
            </button>
          </li>
          <li className="list-group-item">
            Buy Item2 - <span className="font-weight-bold">200 DAI</span>
            <button
              style={{ marginLeft: "10%" }}
              type="button"
              className="btn btn-primary pull-right"
              onClick={() => buy(ITEMS[1])}
            >
              Buy
            </button>
          </li>
        </ul>
      </div>
    </div>
  );
}

export default App;
